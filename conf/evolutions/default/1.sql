# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table student (
  mssv                      varchar(255) not null,
  name                      varchar(255),
  birthday                  varchar(255),
  adress                    varchar(255),
  phone                     bigint,
  email                     varchar(255),
  pass                      varchar(255),
  khóa                      varchar(255),
  ngành                     varchar(255),
  constraint pk_student primary key (mssv))
;

create sequence student_seq;




# --- !Downs

drop table if exists student cascade;

drop sequence if exists student_seq;

