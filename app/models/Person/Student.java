package models.Person;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wellfrog on 2/9/2015.
 */
@Entity
public class Student extends Model {
    @Id
    public  String MSSV;
    public  String name;
    public String birthday;
    public  String adress;
    public  long phone;
    public  String email;
    public  String pass;
    public  String khóa;
    public  String ngành;
    public static List<Student> students;

//    static {
//
//        students = new ArrayList<Student>();
//
//        students.add(new Student("13020412","Phan Hoang Tung", new Birthday(2, 3, 1995), " Ha Noi", 1234567890, "x@gmail.com", "123456", "k58", "CNTT"));
//
//        students.add(new Student("13020422","Nguyễn Hoàng Hải", new Birthday(8, 7, 1994), " Ha Nam", 1234567890, "y@gmail.com", "123456", "k58", "CNTT"));
//
//        students.add(new Student("13020421","Khuất Thanh Sơn", new Birthday(2, 3, 1995), " Ha Tay", 1234567890, "z@gmail.com", "123456", "k58", "CNTT"));
//
//        students.add(new Student("13020424","Trần Vũ Duy", new Birthday(2, 3,1995), " Hai Duong", 1234567890, "t@gmail.com", "123456", "k58", "CNTT"));
//
//        students.add(new Student("13020423","Lê Văn Đàn", new Birthday(2, 3, 1995), " Bac Ninh", 1234567890, "u@gmail.com", "123456", "k58", "CNTT"));
//
//    }
    public static List<Student> findAll(){
        return new ArrayList<Student>(students);
    }
    public Student(String MSSV, String name, String birthday, String adress, long phone, String email, String pass, String khóa, String ngành) {
        this.MSSV=MSSV;
        this.name = name;
        this.birthday = birthday;
        this.adress = adress;
        this.phone = phone;
        this.email = email;
        this.pass = pass;
        this.khóa = khóa;
        this.ngành = ngành;
    }
    public static Finder<String,Student> find=new Finder<>(String.class,Student.class);

    public static boolean remove(Student product) {

        return students.remove(product);

    }




    public  String toString(){
        return name +"---" + MSSV ;
    }
}

