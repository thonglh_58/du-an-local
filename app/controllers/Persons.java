package controllers;

import models.Person.Student;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Result;
import play.mvc.Controller;
import views.html.Login.login;
import views.html.actions.details;
import views.html.actions.list;
import views.html.actions.newStudent;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by wellfrog on 2/9/2015.
 */
public class Persons extends Controller {
    public static Result login() {
        return ok(login.render());
    }

    public static Result details(String MSSV) {
        Student student=Student.find.byId(MSSV);
        return ok(details.render(student));
    }

    public static Result list() {
        List<Student> students = Student.find.all();
        return ok(list.render(students));
        //return TODO;
    }

    public static Result save() {
        DynamicForm df = play.data.Form.form().bindFromRequest();
        if(Student.find.byId(df.get("MSSV"))==null){
            String phonex=df.get("phone");
            Student x=new Student(df.get("MSSV"), df.get("name"), df.get("day")+"/"+df.get("month")+"/"+df.get("year"), df.get("Adress"), Long.parseLong(phonex), df.get("email") ,df.get("pass") ,df.get("Khóa") , df.get("Ngành") );
            x.save();
            flash("success", String.format("Successfully added product %s", x));
            return redirect(routes.Persons.list());
        }
        flash("error", "MSSV da ton tai");
        return badRequest(newStudent.render());
        //return ok(df.get("phone"));
    }

    public static Result newPerson() {
        return ok(newStudent.render());
    }
    public static Result delete(String MSSV){
        Student delete_student = Student.find.byId(MSSV);
        delete_student.delete();
        return redirect(routes.Persons.list());
    }
}
// Nguyen ly 2, thu 2 tiet 6-8